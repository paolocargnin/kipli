(function() {
  'use strict';

  angular
    .module('kipli')
    .directive('kipli', function(){
      return {
        // restrict: "AEC",
        scope: {
          status: "@status",
          name: "@name"
        },
        templateUrl: 'app/components/monsters/kipli.html'
      };
    });
})();
