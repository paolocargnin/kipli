(function() {
  'use strict';

  angular
    .module('kipli')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/monsters/', {
        templateUrl: 'app/components/monsters/monsters.html',
        controller: 'MonstersController',
        controllerAs: 'monsters'
      })
      .when('/classic/', {
        templateUrl: 'app/classic/classic.html',
        controller: 'ClassicGameController',
        controllerAs: 'classic'
      })
      .when('/classic/:level', {
        templateUrl: 'app/game/game.html',
        controller: 'GameController',
        controllerAs: 'game'
      })
      .when('/kipli-game/:level', {
        templateUrl: 'app/kipligame/game.html',
        controller: 'KipliGameController',
        controllerAs: 'game'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
