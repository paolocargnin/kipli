(function() {
  'use strict';

  angular
    .module('kipli')
    .controller('MonstersController', MonstersController);


  /** @ngInject */
  function MonstersController() {
    var vm = this;
    vm.kiplis=[{
      className: 'one',
      status : ''
    },{
        className: 'two',
        status : ''
      },{
        className: 'three',
        status : ''
      },{
        className: 'five',
        status : ''
      },{
        className: 'four',
        status : ''
      }];
    vm.changeStauts = function(kipli){
      kipli.status = kipli.status == '' ? 'selected' : '';
    }

  }
})();
