/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('kipli')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
