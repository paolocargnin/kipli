(function() {
  'use strict';

  angular
    .module('kipli')
    .controller('ClassicGameController', ClassicGameController);

  /** @ngInject */
  function ClassicGameController() {
    var vm = this;

    vm.title="Classic game";

    vm.levels = [{
      level: 1
    }];

    for (var i = 1; i <= 30; i++) {
      vm.levels.push({
        level : i
      });
    }
  }
})();
