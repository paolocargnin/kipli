(function() {
  'use strict';

  angular
    .module('kipli', ['ngTouch', 'ngMessages', 'ngResource', 'ngRoute', 'toastr']);

})();
