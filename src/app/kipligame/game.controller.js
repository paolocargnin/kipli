(function() {
  'use strict';

  angular
    .module('kipli')
    .controller('KipliGameController', GameController);

  /** @ngInject */
  function GameController($interval,$timeout,$log) {
    var vm = this;


    vm.totalSequences = 18; // Difficoltà (Alzare con l'aumento dei livelli)
    vm.sequenceTimeout = 8500; // Difficoltà
    vm.singleSequenceTime = vm.sequenceTimeout; // Difficoltà (abbassare con l'aumento dei livelli)
    vm.gentlyRatio = .8;

    vm.kiplis = [ {
      kipliName:'one',
      isPulsing:false
    } ,
    {
      kipliName:'two',
      isPulsing:false
    }, 
    {
      kipliName:'three',
      isPulsing:false
    }, 
    {
      kipliName:'four',
      isPulsing:false
    }, 
    {
      kipliName:'five',
      isPulsing:false
    }];


    vm.animationGoAwayRatio=0.25; // Difficoltà
    vm.minSequenceAnimationDuration = 1000; // Difficoltà
    

    //Variabili di punteggio  (forse Statici Forse NO)
    vm.timeRatioToCompleteWithNoLossOfPoints = 0.44; // Ratio del tempo in qui non perdi punteggio se lo completi

    //valori forse Statici forse No
    vm.maxPointsPerColor = 300;
    vm.minPointsPerColor = 50;
    vm.colorsForSequence = 4;


    //valori statici

    vm.points= 0;
    vm.maxPoints = 0;

    vm.sequenceDeleteAnimation = 500;

    vm.timeToCompleteWithNoLossOfPoints = parseInt(vm.sequenceTimeout * vm.timeRatioToCompleteWithNoLossOfPoints);

    vm.time = vm.totalSequences * vm.singleSequenceTime * vm.gentlyRatio;

    if (vm.sequenceTimeout*vm.animationGoAwayRatio > 1000){
      vm.sequenceAnimationDuration =  vm.sequenceTimeout*vm.animationGoAwayRatio;
      vm.sequenceDelayAnimation = vm.sequenceTimeout *(1 - vm.animationGoAwayRatio);
    }else{
      vm.sequenceAnimationDuration = 1000;
      vm.sequenceDelayAnimation = vm.sequenceTimeout - 1000;
    }

    //rendo i due valori css appetibili
    vm.sequenceAnimationDuration = vm.sequenceAnimationDuration / 1000;
    vm.sequenceDelayAnimation = vm.sequenceDelayAnimation / 1000;


    vm.sequences = [];
    vm.sequenceSelected = '';

    vm.freePositions = ['1','2','3','4','5'];

    vm.sequenceToDo = vm.totalSequences;

    // Aggingitore di sequenze
    // Posso aggiungere max 2 (n) sequenze in 4000(x) millisecondi, e il controll lo faccio ogni 500(y) ms
    vm.sessionTime = vm.time/5; //ci sono 5 sessioni
    vm.intervalAdding = 1000
    vm.intervalPerSession = vm.sessionTime/vm.intervalAdding;
    vm.sequencesPerSession = parseInt(vm.totalSequences/5)+1;
    vm.addingSession = {
      id : parseInt( Math.random() * 10000 ),
      sequences: 0
    }
    $interval(function(){
      // probabilità dell'apparire:
      var randomNumber = parseInt(Math.random() * vm.intervalPerSession) + 1;
      if ( 
          vm.addingSession.sequences < vm.sequencesPerSession // non ce ne sono da aggiungere
          && randomNumber <= vm.sequencesPerSession //quindi ha battuto il controllo della probabilità
          && vm.sequences.length < 5 //non ci sono più posti liberiiii 
          && ! vm.gameFinished // IL gioco è finito. Cazzo fai?
        ){
        //allora agggiungo
        var newSequence = vm.generateSequence(vm.addingSession.id);
        
        vm.sequences.push(newSequence);

        $timeout(function(){
          vm.deleteSequence(newSequence);
        },vm.sequenceTimeout);
      }
    } , vm.intervalAdding);
    $interval(function(){
      vm.addingSession = {
        id : parseInt( Math.random() * 10000 ),
        sequences: 0
      }
    } , vm.sessionTime);


    $timeout(function(){
       vm.endGame(true);
    },vm.time);

    vm.gameFinished = false;

    vm.generateSequence = function(session){
      var sequenceLen = parseInt( Math.random() * vm.colorsForSequence )+1, //the length of sequence
        kiplis=[],
        randomPositionIndex = Math.floor(Math.random()*vm.freePositions.length);

      for (var i = sequenceLen - 1; i >= 0; i--) { 
        kiplis.push({
          kipliName: vm.kiplis[Math.floor(Math.random()*vm.kiplis.length)].kipliName,
          selected: false,
          isPulsing: false
        }); //a random color from colors
      }
      var sequence = {
        id: parseInt(Math.random()*9999),
        addingSession: session,
        kiplis : kiplis,
        complete:false,
        position: vm.freePositions[randomPositionIndex],
        createdAt : new Date() // per calcolare il punteggio
      }
      vm.freePositions.splice(randomPositionIndex, 1);
      return sequence;
    }

    vm.selectSequence = function(sequence){
      if (vm.sequenceSelected.id == sequence.id ){
        vm.sequenceSelected = ''
      }else{
        vm.sequenceSelected = sequence;
      }
    }

    vm.touchTheButton = function(colorClicked){
      colorClicked.isPulsing = true;
      $timeout(function(){
        colorClicked.isPulsing = false;
      },150);
      var nextColor = _.filter(vm.sequenceSelected.kiplis,function(kipli){
        return ! kipli.selected;
      });
      if ( nextColor[0]!=undefined && colorClicked.kipliName == nextColor[0].kipliName ){
        nextColor[0].selected=true;
  
        if (nextColor.length==1){ //se la lunghezza di next-color era uno allora è finita
          vm.completeSequence(vm.sequenceSelected);
        }
      }
    };

    vm.checkKipliStatus = function(kipli,sequence){
      if (kipli.selected == true){
        return "killed";
      }
      if (vm.sequenceSelected.id == sequence.id){
        return "selected";
      }
      return "";
    }


    vm.completeSequence = function(sequenceToRemove){
      //Sequenza terminata con successo
      $timeout(function(){
        vm.removeSequence(sequenceToRemove);
      },vm.sequenceDeleteAnimation);

      //calcolo i punti da aggiungere così:
      var points = 0;

      var sequenceTimeEnd = new Date();
      var sequenceDuration = sequenceTimeEnd - sequenceToRemove.createdAt;
      var maxPointsForThisSequence = vm.maxPointsPerColor * sequenceToRemove.kiplis.length;
      if (  ( vm.sequenceTimeout - sequenceDuration )  >=  ( vm.sequenceTimeout - vm.timeToCompleteWithNoLossOfPoints )  ){
        points = maxPointsForThisSequence ;
      }else{
        // 6800 - 2800 : 300*numeroColori = 6800- sequenceDuration   : x  
        // ( vm.sequenceTimeout - vm.timeToCompleteWithNoLossOfPoints ) : ( vm.maxPointsPerColor - vm.minPointsPerColor ) = ( vm.sequenceTimeout - sequenceDuration )  : x
        points = ( ( vm.sequenceTimeout - sequenceDuration ) * maxPointsForThisSequence ) /  (vm.sequenceTimeout - vm.timeToCompleteWithNoLossOfPoints );
        points = parseInt(points);
      }

      vm.points = vm.points + points;
      vm.maxPoints = vm.maxPoints + maxPointsForThisSequence;
      $log.log('Sequence end with complete: ' + maxPointsForThisSequence + "=>" + vm.maxPoints );
    }

    vm.deleteSequence = function(sequenceToRemove){
      vm.removeSequence(sequenceToRemove);

      //Sequenza terminata perché il tempo è scaduto
      //Bisognerebbe comunque aggiungere i punti per i colori completi
      //Ora no però
      var maxPointsForThisSequence = vm.maxPointsPerColor * sequenceToRemove.colors.length;
      vm.maxPoints = vm.maxPoints + maxPointsForThisSequence/2;
      $log.log('Sequence end with delete: ' + maxPointsForThisSequence + "=>" + vm.maxPoints );
    }

    vm.removeSequence = function(sequenceToRemove){
      if ( ! sequenceToRemove.completed){
        vm.freePositions.push(sequenceToRemove.position);
        _.remove(vm.sequences,function(sequence){
          return sequenceToRemove.id == sequence.id;
        });
        if (vm.sequenceToDo == 1){ //bug? maybe
          vm.endGame();
        }
        vm.sequenceToDo --;
      }
      sequenceToRemove.completed = true;
    }

    vm.endGame = function(forTheTime){
      if (forTheTime != undefined){
        vm.percentualPoints = vm.points / ( vm.maxPoints + ( vm.sequenceToDo * vm.colorsForSequence * vm.maxPointsPerColor ) )
        vm.message = "Hai Finito i secondi cazzo";
      }else{
        vm.percentualPoints  = vm.points / vm.maxPoints;                
        vm.message = "";
      }

      vm.gameFinished = true;
    };

    vm.restartGame = function(){
      location.reload();
    };

  }
})();
